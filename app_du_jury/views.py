import pyrebase 
from django.shortcuts import render,redirect

config = {
 	'apiKey': "AIzaSyBq3cx_XRaOWrZ4weLZ6jwoxY8Q1CWoHaQ",
    'authDomain': "app-du-jury.firebaseapp.com",
    'databaseURL': "https://app-du-jury.firebaseio.com",
    'projectId': "app-du-jury",
    'storageBucket': "app-du-jury.appspot.com",
    'messagingSenderId': "725123788821",
    'appId': "1:725123788821:web:8ef6317da3e42ac2705e1c"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
db = firebase.database()


def home(request):
	return render(request, "accueil.html")

def candidat(request): 
	Candidat = db.child('candidats').get()
	dataList = []
	for list in Candidat.each():
		result = {
			'key' : list.key(),
			'candidat' : list.val(),
		}		
		dataList.append(result)


	return render(request,'accueil.html', {'datas': dataList})


def addCandidat(request):
	data = {
		'name' : request.POST.get('name'),
		'titrePro' : request.POST.get('titlePro')
	}	
	db.child('candidats').push(data)

	return redirect('/candidat/')

def evaluation(request):
	key = request.POST.get('key')
	name = request.POST.get('name')
	# data = {
	# 	'key' : key,
	# 	'name' : name,
	# }
	
	return render(request,'evaluation.html', {'name' : name, 'key': key})

def post_eval(request) :
	Competences = {
		'comp1' : request.POST.get('comp1'),
		'comp2' : request.POST.get('comp2'),
		'comp3' : request.POST.get('comp3'),
		'comp4' : request.POST.get('comp4'),
		'comp5' : request.POST.get('comp5'),
		'comp6' : request.POST.get('comp6'),
		'comp7' : request.POST.get('comp7'),
		'comp8' : request.POST.get('comp8'),
		'userID' : request.POST.get('key'),
	}
	
	db.child('competences').push(Competences)
	return redirect('/resume/')

def resume(request): 
	Competences = db.child('competences').get()
	dataList = []
	for list in Competences.each():
		result = {
			'comp' : list.val(),
			'userID' : list.val().filter('userID'),
		}		
		dataList.append(result)
	return render(request,'resume.html', {'comp':dataList})