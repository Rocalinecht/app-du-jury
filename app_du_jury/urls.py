from django.contrib import admin
from django.conf.urls import url
from . import views
urlpatterns = [
url(r'^admin/', admin.site.urls),
url(r'^$',views.home),
# url(r'^newcandidat/',views.newCandidat),
url(r'^candidat/',views.candidat),
url(r'^addCandidat/', views.addCandidat),
url(r'^evaluation/', views.evaluation),
url(r'^post_eval/', views.post_eval),
url(r'^resume/', views.resume),
]